<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 1/4/17
 * Time: 12:14 AM
 */

require_once('lib/DB.php');


//Initiation of DB object
$db = new DB();

//Search Pattern Setup
$response = $db->simpleQuery(
    'item',               			# Table
    array( '*' ),           		# Fields
    array(                  		# Filters

    )
);

$duplicated = array();

for ($i=0; $i<count($response); $i++) {

    for ($j=0; $j<count($response); $j++) {

        if ( $response[$i]['id'] != $response[$j]['id'] && $response[$i]['item_id'] == $response[$j]['item_id'] ){

            //echo $response[$i]['item_id'] . '</br>';
            //echo $response[$j]['item_id'] . '</br>';

            array_push($duplicated, $response[$j]['id']);


        }

        $j++;
    }

    $i++;
}

//var_dump($duplicated);die;

foreach ( $duplicated as $id ) {

    echo 'Delete: ' . $id . '</br>';

    $db->delete(
        'item',                     # Table
        array( 'id' => $id )  # Filters
    );

}