<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 1/3/17
 * Time: 8:35 PM
 */


//This files contains all variables in the system

//Database Config
$GLOBALS['DB_HOST'] = 'localhost';
$GLOBALS['DB_NAME'] = 'smartbuy';
$GLOBALS['DB_USER'] = 'root';
$GLOBALS['DB_PASSWD'] = 'quetalroman';

//Find pattern
$GLOBALS['PATTERNS'] = array('/(\d\d\d\d\d\d)-(\d\d\d)/', '/(\d\d\d\d\d\d\d\d\d)/');
$GLOBALS['FILTER_PATTERNS'] = array('hp motherboard');

//Ebay Config
$GLOBALS['find_url'] = 'http://svcs.ebay.com/services/search/FindingService/v1?';
$GLOBALS['shop_url'] = 'http://open.api.ebay.com/shopping?';
$GLOBALS['app_id'] = 'RomanMVa-smartbuy-PRD-02f871c7c-da558d49';
$GLOBALS['global_id'] = 'EBAY-US';
$GLOBALS['version'] = '1.2.0';
$GLOBALS['format'] = 'json';
$GLOBALS['limit'] = 100;


//Ebay URL using "hp motherboard" pattern and 200 item per page
$GLOBALS['ebay_hp_m'] = array("http://www.ebay.com/sch/i.html?LH_Complete=1&LH_Sold=1&_nkw=hp+motherboard&_ipg=200&rt=nc",
                            "http://www.ebay.com/sch/i.html?LH_Complete=1&LH_Sold=1&_nkw=hp+motherboard&_pgn=2&_skc=200&rt=nc",
                            "http://www.ebay.com/sch/i.html?LH_Complete=1&LH_Sold=1&_nkw=hp+motherboard&_pgn=3&_skc=200&rt=nc",
                            "http://www.ebay.com/sch/i.html?LH_Complete=1&LH_Sold=1&_nkw=hp+motherboard&_pgn=4&_skc=200&rt=nc",
                            "http://www.ebay.com/sch/i.html?LH_Complete=1&LH_Sold=1&_nkw=hp+motherboard&_pgn=5&_skc=200&rt=nc",
                            "http://www.ebay.com/sch/i.html?LH_Complete=1&LH_Sold=1&_nkw=hp+motherboard&_pgn=6&_skc=200&rt=nc",
                            "http://www.ebay.com/sch/i.html?LH_Complete=1&LH_Sold=1&_nkw=hp+motherboard&_pgn=7&_skc=200&rt=nc",
                            "http://www.ebay.com/sch/i.html?LH_Complete=1&LH_Sold=1&_nkw=hp+motherboard&_pgn=8&_skc=200&rt=nc",
                            "http://www.ebay.com/sch/i.html?LH_Complete=1&LH_Sold=1&_nkw=hp+motherboard&_pgn=9&_skc=200&rt=nc",
                            "http://www.ebay.com/sch/i.html?LH_Complete=1&LH_Sold=1&_nkw=hp+motherboard&_pgn=10&_skc=200&rt=nc" );
