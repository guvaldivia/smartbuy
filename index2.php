<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 1/4/17
 * Time: 12:14 AM
 */

require_once('lib/DB.php');
require_once('lib/utils.php');
require_once('config.php');

//Initiation Utils tools
$utils = new Utils();

//Initiation of DB object
$db = new DB();


foreach ($GLOBALS['FILTER_PATTERNS'] as $pattern) {

    //Search Pattern Setup
    $response = $db->simpleQuery(
        'search_pattern',               # Table
        array( '*' ),           		# Fields
        array(                  		# Filters
            'description' => $pattern
        )
    );

    if ( count($response) == 0 ) {

        $search_pattern_id = $utils->generateId();

        $response = $db->insert('search_pattern',       # Table
            array(                  					# Insert
                'id' => $search_pattern_id,
                'description' => $pattern
            )
        );
    } else {
        $search_pattern_id = $response[0]['id'];
    }


    //Insert the search defines into DB
    $search_id = $utils->generateId();
    $real_items = 0;

    $db->insert('search',					       		# Table
        array(                  						# Insert
            'id' => $search_id,
            'search_pattern_id' => $search_pattern_id,
            'count_items' => $real_items
        )
    );


    $items = $utils->getItemsFromHpMotherboard();

    echo "Total Items: " . count($items) . "<br>";

    $count_item = 1;

    foreach($items as $item){

        $item_is_new = false;

        echo "Item: " . $count_item . ", Item ID: " . $item['itemId'] . "<br>";
        $count_item++;

        $endTime = date_create($item['itemDate']);
        $endTime = date_format($endTime, "Y-m-d H:i:s");


        //Search Pattern Setup
        $response = $db->simpleQuery(
            'item',               			# Table
            array( '*' ),           		# Fields
            array(                  		# Filters
                'item_id' => $item['itemId']
            )
        );

        if ( count($response) == 0 ) {

            $item_is_new = true;
        }

        //Condition into DB
        $response = $db->simpleQuery(
            'condition_type',            	# Table
            array( '*' ),           		# Fields
            array(                  		# Filters
                'description' => $item['itemCondition']
            )
        );

        if ( count($response) == 0 ) {

            $condition_id = $utils->generateId();

            $response = $db->insert('condition_type',       # Table
                array(                  					# Insert
                    'id' => $condition_id,
                    'description' => $item['itemCondition']
                )
            );
        } else {
            $condition_id = $response[0]['id'];
        }

        //Seller into DB
        $response = $db->simpleQuery(
            'seller', 	            		# Table
            array( '*' ),           		# Fields
            array(                  		# Filters
                'user' => $item['itemSeller']
            )
        );

        if ( count($response) == 0 ) {

            $seller_id = $utils->generateId();

            $response = $db->insert('seller',       		# Table
                array(                  					# Insert
                    'id' => $seller_id,
                    'user' => $item['itemSeller'],
                    'feedback' => '',
                    'items_sold' => ''
                )
            );
        } else {
            $seller_id = $response[0]['id'];
        }

        //Gets Part Number
        $part_number = $utils->getPartNumber($item['itemTitle']);

        //Part Number into DB
        $response = $db->simpleQuery(
            'part_number',            	# Table
            array( '*' ),           		# Fields
            array(                  		# Filters
                'description' => $part_number
            )
        );

        if ( count($response) == 0 ) {

            $part_number_id = $utils->generateId();

            $response = $db->insert('part_number',       	# Table
                array(                  					# Insert
                    'id' => $part_number_id,
                    'description' => $part_number
                )
            );
        } else {
            $part_number_id = $response[0]['id'];
        }


        //Insert Product into Item Table
        $product_id = $utils->generateId();

        if ( $item_is_new ) {

            $response = $db->insert('item',       			# Table
                array(                  					# Insert
                    'id' => $product_id,
                    'part_number_id' => $part_number_id,
                    'condition_type_id' => $condition_id,
                    'seller_id' => $seller_id,
                    'search_pattern_id' => $search_pattern_id,
                    'search_id' => $search_id,
                    'title' => $item['itemTitle'],
                    'item_id' => $item['itemId'],
                    'shipping' => '0',
                    'start_time' => $utils->createDateMinus2Month($endTime),
                    'end_time' => $endTime,
                    'quantity' => '0',
                    'quantity_sold' => $item['itemSolds'],
                    'hit_count' => '0',
                    'return_accepted' => '1',
                    'price' => $item['itemPrice'],
                    'currency' => $item['itemCurrency']
                )
            );

        }
        else {

            //Updating real counter items
            $db->update('item',		# Table
                array(
                    'part_number_id' => $part_number_id,
                    'condition_type_id' => $condition_id,
                    'seller_id' => $seller_id,
                    'search_pattern_id' => $search_pattern_id,
                    'search_id' => $search_id,
                    'end_time' => $endTime,
                    'quantity' => '0',
                    'quantity_sold' => $item['itemSolds'],
                    'price' => $item['itemPrice'],
                    'currency' => $item['itemCurrency']
                ),
                array(                  # Filters
                    'item_id' => $item['itemId'],
                )
            );

        }

        //Adding real items to the counter
        $real_items++;


        /*echo "------------------------------------INICIO-------------------------------------------------</br>";
        echo "Title: " . $item['itemTitle'] . "<br>";
        echo "Item Id: " . $item['itemId'] . "<br>";
        echo "Current Price: " . $item['itemPrice'] . " " . $item['itemCurrency'] . "<br>";
        echo "Date: " . $item['itemDate'] . "<br>";
        echo "Condition: " . $item['itemCondition'] . "<br>";
        echo "Seller: " . $item['itemSeller'] . "<br>";
        echo "Solds: " . $item['itemSolds'] . "<br>";
        echo "-------------------------------------FIN---------------------------------------------------</br>";*/


    }


    //Updating real counter items
    $db->update('search',		# Table
        array(
            'count_items' => $real_items
        ),
        array(                  # Filters
            'id' => $search_id
        )
    );



}


