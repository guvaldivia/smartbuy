var dataTable;

$(document).ready(function() {

    $("#title").val("");
    $("#part_number").val("");
    $("#condition").val("");
    $("#seller").val("");
    $("#search_pattern").val("");
    $("#search").val("");
    $("#start_time").val("");
    $("#end_time").val("");
    $("#quantiy").val("");
    $("#price").val("");
    $("#returns").val("");

    dataTable = $('#products-grid').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax":{
            url :"basic/products-grid-data.php", // json datasource
            type: "post",  // method  , by default get
            error: function(){  // error handling
                $(".products-grid-error").html("");
                $("#products-grid").append('<tbody class="products-grid-error"><tr><th colspan="6">No data found in the server</th></tr></tbody>');
                $("#products-grid_processing").css("display","none");

            }
        },
        "order": [[ 4, 'desc' ]]
    } );

    $("#products-grid_filter").css("display","none");

    initFilters();

    function initFilters() {

        var part_number_filter = $("#part_number_get").val();
        if ( part_number_filter != "" ) {
            $("#1").val(part_number_filter);
            dataTable.columns(1).search(part_number_filter).draw();
        }

        var date_appear_filter = $("#date_appear").val();
        if ( date_appear_filter != "" ) {
            $("#6").val(date_appear_filter);
            dataTable.columns(6).search(date_appear_filter).draw();
        }
    }

    $('.products-search-input').on( 'keyup click change', function () {
        var i =$(this).attr('id');  // getting column index
        var v =$(this).val();  // getting search input value
        dataTable.columns(i).search(v).draw();
    } );

    $( ".datepicker" ).datepicker({
        dateFormat: "mm/dd/yy",
        //showOn: "button",
        showAnim: 'slideDown',
        showButtonPanel: true ,
        autoSize: true,
        //buttonImage: "//jqueryui.com/resources/demos/datepicker/images/calendar.gif",
        //buttonImageOnly: true,
        buttonText: "Select date",
        closeText: "Clear",
        inline: true
        //altField: '.datepicker'
    });

    $(document).on("click", ".ui-datepicker-close", function(){
        $('.datepicker').val("");
        var i =$(this).attr('id');  // getting column index
        var v =$(this).val();  // getting search input value
        dataTable.columns(i).search(v).draw();
    });

    $(document).on("click", ".info", function(){
        var id =$(this).attr('data-value');

        $.ajax({
            url : 'basic/details.php',
            data : { id : id },
            type : 'POST',
            dataType : 'json',
            success : function(response) {
                if ( response.success ) {

                    $("#title").val(response.data.title);
                    $("#part_number").val(response.data.part_number);
                    $("#condition").val(response.data.condition_type);
                    $("#seller").val(response.data.seller);
                    $("#search_pattern").val(response.data.search_pattern);
                    $("#search").val(response.data.search);
                    $("#start_time").val(response.data.start_time);
                    $("#end_time").val(response.data.end_time);
                    $("#quantiy").val(response.data.quantiy);
                    $("#price").val(response.data.price);
                    $("#returns").val(response.data.returns);
                    $("#currency").val(response.data.currency);

                    $("#myInfoModal").modal('show');
                }
                else
                    alert(response.msg);
            },
            error : function(xhr, status) {
                alert('Sorry, there was a problem!!');
            },
            async:false
        });
    });

    $(document).on("click", "#btnFilter", function(){
        var filterFrom = $('#filterFrom').val();
        var filterTo = $('#filterTo').val();

        dataTable.columns(6).search(filterFrom+"-"+filterTo).draw();
    });

} );



function alertDelete(id) {
    {
        var r=confirm("Really, do yo want remove this product!!");
        if (r==true)
        {
            $.ajax({
                url : 'basic/remove-products.php',
                data : { id : id },
                type : 'POST',
                dataType : 'json',
                success : function(response) {
                    if ( response.success )
                        dataTable.draw();
                    else
                        alert(response.msg);
                },
                error : function(xhr, status) {
                    alert('Sorry, there was a problem!!');
                },
                async:false
            });
        }
    }
};

function editPartNumber(id, description) {
    {
        description=prompt("Part Number: ",description);

        if (description!=null)
        {
            $.ajax({
                url : 'basic/edit-part-number.php',
                data : { id : id,
                    description: description},
                type : 'POST',
                dataType : 'json',
                success : function(response) {
                    if ( response.success )
                        dataTable.draw();
                    else
                        alert(response.msg);
                },
                error : function(xhr, status) {
                    alert('Sorry, there was a problem!!');
                },
                async:false
            });
        }
    }
};