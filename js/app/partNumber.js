/**
 * Created by roman on 1/11/17.
 */
var dataTable;

$(document).ready(function() {

    dataTable = $('#partnumbers-grid').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax":{
            url :"partNumber/partnumbers-grid-data.php", // json datasource
            type: "post",  // method  , by default get
            error: function(){  // error handling
                $(".partnumbers-grid-error").html("");
                $("#partnumbers-grid").append('<tbody class="partnumbers-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                $("#partnumbers-grid_processing").css("display","none");

            }
        },
        "order": [[ 4, 'desc' ]],
        "columnDefs": [
            { "orderable": false, "targets": 1 },
            { "orderable": false, "targets": 2 }
        ]
    } );

    $("#partnumbers-grid_filter").css("display","none");

    $('.partnumber-search-input').on( 'keyup click change', function () {
        var i =$(this).attr('id');  // getting column index
        var v =$(this).val();  // getting search input value
        dataTable.columns(i).search(v).draw();
    } );

    $( ".datepicker" ).datepicker({
        dateFormat: "mm/dd/yy",
        //showOn: "button",
        showAnim: 'slideDown',
        showButtonPanel: true ,
        autoSize: true,
        //buttonImage: "//jqueryui.com/resources/demos/datepicker/images/calendar.gif",
        //buttonImageOnly: true,
        buttonText: "Select date",
        closeText: "Clear",
        inline: true
        //altField: '.datepicker'
    });

    $(document).on("click", ".ui-datepicker-close", function(){
        $('.datepicker').val("");
        var i =$(this).attr('id');  // getting column index
        var v =$(this).val();  // getting search input value
        dataTable.columns(i).search(v).draw();
    });

    $(document).on("click", "#btnFilter", function(){
        var filterFrom = $('#filterFrom').val();
        var filterTo = $('#filterTo').val();

        dataTable.columns(2).search(filterFrom+"-"+filterTo).draw();
    });


} );
