/**
 * Created by roman on 1/11/17.
 */
var dataTable;

$(document).ready(function() {

    dataTable = $('#searchs-grid').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax":{
            url :"search/searchs-grid-data.php", // json datasource
            type: "post",  // method  , by default get
            error: function(){  // error handling
                $(".searchs-grid-error").html("");
                $("#searchs-grid").append('<tbody class="searchs-grid-error"><tr><th colspan="3">No data found in the server</th></tr></tbody>');
                $("#searchs-grid_processing").css("display","none");

            }
        },
        "order": [[ 1, 'desc' ]]
    } );

    $("#searchs-grid_filter").css("display","none");


} );
