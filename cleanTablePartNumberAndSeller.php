<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 1/4/17
 * Time: 12:14 AM
 */

require_once('lib/DB.php');


//Initiation of DB object
$db = new DB();

//Search Pattern Setup
$response = $db->simpleQuery(
    'part_number',               			# Table
    array( '*' ),           		# Fields
    array(                  		# Filters

    )
);

foreach ($response as $part_number) {
    $db->delete(
        'part_number',                                 # Table
        array( 'id' => $part_number['id'] )             # Filters
    );
}


//Search Pattern Setup
$response = $db->simpleQuery(
    'seller',               			# Table
    array( '*' ),           		# Fields
    array(                  		# Filters

    )
);

foreach ($response as $seller) {
    $db->delete(
        'seller',                                 # Table
        array( 'id' => $seller['id'] )             # Filters
    );
}