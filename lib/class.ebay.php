<?php

require_once (dirname(dirname(__FILE__)).'/config.php');

class Ebay{

	private $find_url = null;
	private $shop_url = null;
	private $app_id = null;
	private $global_id = null;
	private $version = null;
	private $format = null;

	private $limit = 0;

	public function __construct(){

	    $this->find_url = $GLOBALS['find_url'];
        $this->shop_url = $GLOBALS['shop_url'];
        $this->app_id = $GLOBALS['app_id'];
        $this->global_id = $GLOBALS['global_id'];
        $this->version = $GLOBALS['version'];
        $this->format = $GLOBALS['format'];
        $this->limit = $GLOBALS['limit'];


	}


	public function findItems($keyword = 'robot', $page = 1,  $item_sort = 'EndTimeSoonest', $item_type = 'FixedPrice', $min_price = '20', $max_price = '500'){

		$url  = $this->find_url;
		$url .= 'operation-name=findItemsByKeywords';
		$url .= '&service-version=' . $this->version;
		$url .= '&keywords=' . urlencode($keyword);

        $url .= '&sortOrder=' . $item_sort;

        //$url .= '&itemFilter(0).name=SoldItemsOnly';
        //$url .= '&itemFilter(0).value=true';

        $url .= '&outputSelector=SellerInfo,StoreInfo,UnitPriceInfo';

        $url .= '&paginationInput.entriesPerPage=' . $this->limit;
        $url .= '&paginationInput.pageNumber=' . $page;

        $url .= '&descriptionSearch=false';

        $url .= '&security-appname='. $this->app_id;
        $url .= '&response-data-format=' . $this->format;

		return json_decode(file_get_contents($url), true);

	}

	public function findItemsAdvanced($keyword = 'robot', $item_sort = 'EndTimeSoonest', $item_type = 'FixedPrice', $min_price = '60', $max_price = '9999999'){

		$url  = $this->find_url;
		$url .= 'operation-name=findItemsAdvanced';
		$url .= '&service-version=' . $this->version;
		$url .= '&global-id=' . $this->global_id;
		$url .= '&keywords=' . urlencode($keyword);

		$url .= '&sortOrder=' . $item_sort;

		$url .= '&itemFilter(0).name=ListingType';
		$url .= '&itemFilter(0).value=' . $item_type;

		$url .= '&itemFilter(1).name=MinPrice';
		$url .= '&itemFilter(1).value=' . $min_price;

		$url .= '&itemFilter(2).name=MaxPrice';
		$url .= '&itemFilter(2).value=' . $max_price;

		$url .= '&itemFilter(3).name=Currency';
		$url .= '&itemFilter(3).value=USD';

		//$url .= '&itemFilter(4).name=Condition';
		//$url .= '&itemFilter(4).value(0)=New';

		$url .= '&paginationInput.entriesPerPage=' . $this->limit;
		$url .= '&descriptionSearch=false';

		$url .= '&security-appname='. $this->app_id;
		$url .= '&response-data-format=' . $this->format;

		return json_decode(file_get_contents($url), true);

	}

	public function findItemsCompleted($keyword = 'robot', $page = 1, $item_sort = 'EndTimeSoonest'){

		$url  = $this->find_url;
		$url .= 'operation-name=findCompletedItems';
		$url .= '&service-version=' . $this->version;
		$url .= '&global-id=' . $this->global_id;
		$url .= '&keywords=' . urlencode($keyword);

        $url .= '&itemFilter(0).name=SoldItemsOnly';
        $url .= '&itemFilter(0).value=true';

		$url .= '&sortOrder=' . $item_sort;

		$url .= '&outputSelector=SellerInfo,StoreInfo,UnitPriceInfo';

        $url .= '&paginationInput.entriesPerPage=' . $this->limit;
        $url .= '&paginationInput.pageNumber=' . $page;

		$url .= '&descriptionSearch=false';

		$url .= '&security-appname='. $this->app_id;
		$url .= '&response-data-format=' . $this->format;

		return json_decode(file_get_contents($url), true);

	}

	public function findProductInfo ($product_id) {

		$url  = $this->shop_url;
		$url .= 'callname=GetSingleItem';
		$url .= '&responseencoding=JSON';
		$url .= '&appid=' . $this->app_id;
		$url .= '&siteid=0';
		$url .= '&version=515';

		$url .= '&ItemID=' . $product_id;

		$url .= '&IncludeSelector=Details,Description,TextDescription,ShippingCosts,ItemSpecifics';

		return json_decode(file_get_contents($url), true);

	}

	public function findItemsCompletedBySeller($seller){

		$url  = $this->find_url;
		$url .= 'operation-name=findCompletedItems';
		$url .= '&service-version=' . $this->version;
		$url .= '&global-id=' . $this->global_id;

		$url .= '&itemFilter(0).name=Seller';
		$url .= '&itemFilter(0).value(0)=' . $seller;

		$url .= '&itemFilter(1).name=SoldItemsOnly';
		$url .= '&itemFilter(1).value=true';

		//$url .= '&paginationInput.entriesPerPage=' . 1;
		//$url .= '&descriptionSearch=false';

		$url .= '&security-appname='. $this->app_id;
		$url .= '&response-data-format=' . $this->format;

		return json_decode(file_get_contents($url), true);

	}

}


