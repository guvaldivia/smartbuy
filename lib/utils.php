<?php

/**
 * Created by PhpStorm.
 * User: roman
 * Date: 12/22/16
 * Time: 11:13 AM
 */


require_once (dirname(dirname(__FILE__)).'/config.php');

class Utils
{

    public function __construct(){

    }

    public function generateId() {

        return mt_rand() . '-' . uniqid();

    }

    public function getPartNumber($text) {

       foreach ($GLOBALS['PATTERNS'] as $pattern){
            preg_match($pattern, $text, $matches);

            if (count($matches) > 0 )
                return $matches[0];
        }

        return $text;

    }

    public function getItemsFromHpMotherboard() {
        $result = array();

        foreach ($GLOBALS['ebay_hp_m'] as $url) {

            # Use the Curl extension to query Ebay and get back a page of results
            $ch = curl_init();
            $timeout = 5;
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $html = curl_exec($ch);
            curl_close($ch);

            # Create a DOM parser object
            $dom = new DOMDocument();

            # Parse the HTML from Google.
            # The @ before the method call suppresses any warnings that
            # loadHTML might throw because of invalid HTML in the page.
            @$dom->loadHTML($html);

            # Iterate over all the <li> tags
            foreach($dom->getElementsByTagName('li') as $li) {

                $item = array( 'itemId' => '',
                    'itemTitle' => '',
                    'itemURL' => '',
                    'itemPrice' => '',
                    'itemCurrency' => '',
                    'itemDate' => '',
                    'itemCondition' => '',
                    'itemSeller' => '',
                    'itemSolds' => '');

                $itemId = $li->getAttribute('listingid');

                if ( $itemId !== "") {

                    echo "Item: " . $itemId;

                    $item['itemId'] = $itemId;
                    $item['itemTitle'] = $li->getElementsByTagName('h3')->item(0)->nodeValue;
                    $item['itemURL'] = $li->getElementsByTagName('h3')->item(0)->getElementsByTagName('a')->item(0)->getAttribute('href');
                    $item['itemPrice'] = $li->getElementsByTagName('ul')->item(0)->getElementsByTagName('li')->item(0)->getElementsByTagName('span')->item(0)->nodeValue;
                    $item['itemDate'] = $li->getElementsByTagName('ul')->item(1)->getElementsByTagName('li')->item(0)->getElementsByTagName('span')->item(0)->getElementsByTagName('span')->item(0)->nodeValue;

                    array_push($result, $this->getItemsFromHpMotherboardMoreInfo($item));

                }

            }

        }

        return $result;
    }

    public function getItemsFromHpMotherboardMoreInfo ($item) {

        # Use the Curl extension to query Ebay and get back a page of results
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $item['itemURL']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $html = curl_exec($ch);
        curl_close($ch);

        # Create a DOM parser object
        $dom = new DOMDocument();

        # Parse the HTML from Google.
        # The @ before the method call suppresses any warnings that
        # loadHTML might throw because of invalid HTML in the page.
        @$dom->loadHTML($html);

        $item['itemCondition'] = $dom->getElementById('vi-itm-cond')->nodeValue;

        if ( $dom->getElementById('mbgLink') != NULL )
            if ( $dom->getElementById('mbgLink')->getElementsByTagName('span')->item(0) != NULL )
                $item['itemSeller'] = $dom->getElementById('mbgLink')->getElementsByTagName('span')->item(0)->nodeValue;


        $price = $dom->getElementById('prcIsum')->nodeValue;
        if ( $price == "" )
            $price = $dom->getElementById('mm-saleDscPrc')->nodeValue;
        if ( $price == "" )
            $price = $item['itemPrice'];

        $item['itemPrice'] = $price;

        /*if ( $item['itemDate'] == "" ) {
            $date = $dom->getElementById('bb_tlft')->nodeValue;
            if ( $date != "" )
                if ( substr($date, -1, 1) != ")" )
                    $item['itemDate'] = $date;

        }*/


        try {
            if ( $dom->getElementsByTagName('form')->item(1) != NULL )
                if ( $dom->getElementsByTagName('form')->item(1)->getElementsByTagName('div')->item(0) != NULL ){
                    $form = $dom->getElementsByTagName('form')->item(1)->getElementsByTagName('div')->item(0);
                    if ($form != NULL)
                        if ( $form->getElementsByTagName('div')->item(7) != NULL )
                            if ( $form->getElementsByTagName('div')->item(7)->getElementsByTagName('span') != NULL )
                                $item['itemSolds'] = $form->getElementsByTagName('div')->item(7)->getElementsByTagName('span')->item(1)->nodeValue;
                }


        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }


        $item = $this->formatPrice($item);
        $item = $this->formatSolds($item);
        $item = $this->formatDate($item);

        return $item;
    }

    /*
     * Ej 1: US $79.50
     * Ej 2: $4.99
     * */
    public function formatPrice ($item) {

        $price = $item['itemPrice'];

        $array_price = explode(" ", $price);

        if ( count($array_price) > 1 ) {
            $item['itemCurrency'] = $array_price[0];
            $item['itemPrice'] = substr($array_price[1], 1);
        } else {
            $item['itemCurrency'] = "USD";
            $item['itemPrice'] = substr($array_price[0], 1);
        }

        return $item;
    }


    /*
     *
     * Ej 1: Feb-12 00:11
     * */
    public function formatDate ($item) {

        if ( $item['itemDate'] != "" ) {
            $date = $item['itemDate'];
        } else {
            $month = date("M");
            $day = date("d");
            $hour = date("H:i");

            $date = $month . "-" . $day . " " . $hour;
         }

        $array_date = explode(" ", $date);
        if ( count($array_date) == 2) {

            $array_date1 = explode("-", $array_date[0]);
            $year = date("Y");
            $month = $array_date1[0];
            $day = $array_date1[1];
            $time = $array_date[1] . ":00";

            $date = $year . "-" . $month . "-" . $day . " " . $time;

            $item['itemDate'] = $date;

        }

        return $item;
    }


    /*
     * Ej 1: Last one / 2 sold
     * */
    public function formatSolds ($item) {

        $solds = $item['itemSolds'];

        if ( $solds != "" ) {
            $array_solds = explode("/", $solds);
            if ( count($array_solds) == 2 ) {
                $item['itemSolds'] = filter_var($array_solds[1], FILTER_SANITIZE_NUMBER_INT);
                //var_dump($item['itemSolds']);die;
            } else {
                $item['itemSolds'] = "";
            }
        }

        return $item;
    }

    public function createDateMinus2Month($date) {


        $fecha = date($date);
        $nuevafecha = strtotime ( '-2 month' , strtotime ( $fecha ) ) ;
        $nuevafecha = date ( 'Y-m-d H:i:s' , $nuevafecha );

        return $nuevafecha;

    }

}