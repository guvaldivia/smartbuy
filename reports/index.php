<!DOCTYPE html>
<html>
<title>Ebay Reports</title>
<head>

    <link rel="stylesheet" type="text/css" href="../css/main.css">
    <link rel="stylesheet" type="text/css" href="../css/jquery.dataTables.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <script type="text/javascript" language="javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" language="javascript" src="../js/jquery.dataTables.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="?page=home">SmartBuy Reports</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="?page=home">Home <span class="sr-only">(current)</span></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="?page=basic">Basic Report based on Products</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="?page=part-number">Report based on Part Numbers</a></li>
                        <li><a href="?page=searchs">Report based on Searchs</a></li>
                    </ul>
                </li>
                <li><a href="http://www.ebay.com/sch/allcategories/all-categories">Ebay</a></li>
                <li><a href="?page=about-us">About Us</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>


<?php

if ( $_GET['page'] == 'basic' )
    include_once 'basic/index.php';

elseif ( $_GET['page'] == '' || $_GET['page'] == 'home' || $_GET['page'] == 'index' )
    include_once 'dashboard.php';

elseif ( $_GET['page'] == 'part-number' )
    include_once 'partNumber/index.php';

elseif ( $_GET['page'] == 'searchs' )
    include_once 'search/index.php';

elseif ( $_GET['page'] == 'about-us' )
    include_once 'about-us.php';

else
    include_once 'not_found.php';
?>

<footer class="bd-footer text-muted" style="margin-top: 30px;">
    <div class="container" style="width: 100% !important;">
        <p>Designed and built by GuVal Co. - 2017</p>
        <p>Currently 0.1 Code licensed <a rel="license" href="https://github.com/twbs/bootstrap/blob/master/LICENSE" target="_blank">MIT</a></p>
    </div>
</footer>

</body>
</html>
