<?php

require_once (dirname(dirname(dirname(__FILE__))).'/config.php');

$result = array();


/* Database connection start */
$servername = $GLOBALS['DB_HOST'];
$username = $_DB = $GLOBALS['DB_USER'];
$password = $GLOBALS['DB_PASSWD'];
$dbname = $GLOBALS['DB_NAME'];

$id= $_POST['id'];

$conn = mysqli_connect($servername, $username, $password, $dbname) or die("Connection failed: " . mysqli_connect_error());

$sql = "SELECT title, part_number.description, condition_type.description, seller.user, seller.feedback, seller.items_sold, search_pattern.description, search.date, item.start_time, item.end_time, item.quantity, item.quantity_sold, item.price, item.return_accepted, item.currency FROM item, part_number, condition_type, seller, search_pattern, search WHERE item.id='".$id."' AND item.part_number_id=part_number.id AND item.condition_type_id=condition_type.id AND item.seller_id=seller.id AND item.search_pattern_id=search_pattern.id AND item.search_id=search.id";

$query=mysqli_query($conn, $sql) or die("details.php: get products");

if(! $query ) {
    $result['success'] = false;
    $result['msg'] = 'Could not get data: ' . mysqli_error();
    die;
}

while( $row=mysqli_fetch_array($query) ) {
    $data=array();

    $search = date_create($row[7]);
    $start_date =  date_create($row[8]);
    $end_date =  date_create($row[9]);

    $data['title'] = $row[0];
    $data['part_number'] = $row[1];
    $data['condition_type'] = $row[2];
    $data['seller'] = "$row[3]($row[4]%/$row[5])";
    $data['search_pattern'] = $row[6];
    $data['search'] = date_format($search, "m/d/Y");
    $data['start_time'] = date_format($start_date, "m/d/Y");;
    $data['end_time'] = date_format($end_date, "m/d/Y");;
    $data['quantiy'] = "$row[10]/$row[11]";
    $data['price'] = $row[12];
    $data['returns'] = ($row[13])?"Yes":"No";
    $data['currency'] = $row[14];

    $result['data'] = $data;

    $result['success'] = true;
    $result['msg'] = "Get data successfully";
    echo json_encode($result);
    die;
}


$result['success'] = false;
$result['msg'] = "Could not get data:";
echo json_encode($result);
die;


?>