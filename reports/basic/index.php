<input type="hidden" value="<?php ( array_key_exists('part-number', $_GET) && $_GET['part-number'] != "")?print($_GET['part-number']):"" ?>" id="part_number_get">
<input type="hidden" value="<?php ( array_key_exists('date-appear', $_GET) && $_GET['date-appear'] != "")?print($_GET['date-appear']):"" ?>" id="date_appear">

<div class="header"><h2>Basic Report of Products</h2></div>
<div class="container">

    <div class="panel panel-default">
        <div class="panel-heading">Global Filters</div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="filterFrom">From:</label>
                    <div class="col-sm-6">
                        <input readonly="readonly"  type="text" id="filterFrom" class="datepicker">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="filterTo">To:</label>
                    <div class="col-sm-6">
                        <input readonly="readonly"  type="text" id="filterTo" class="datepicker">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-default" id="btnFilter">Filter</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <table id="products-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
        <thead>
            <tr>
                <th>Title</th>
                <th>Part Number</th>
                <th>Open</th>
                <th>Closed</th>
                <th>Solds</th>
                <th>Price</th>
                <th>Saved</th>
                <th>Actions</th>
            </tr>
        </thead>
        <thead>
            <tr>
                <td><input type="text" id="0"  class="products-search-input"></td>
                <td><input type="text" id="1" class="products-search-input"></td>
                <td valign="middle"><input style="width: 80px;" readonly="readonly"  type="text" id="2" class="products-search-input datepicker"></td>
                <td valign="middle"><input style="width: 80px;" readonly="readonly"  type="text" id="3" class="products-search-input datepicker"></td>
                <td><input style="width: 40px;" type="text" id="4" class="products-search-input" ></td>
                <td>
                    <select id="5"  class="products-search-input">
                        <option value="">0-999</option>
                        <option value="0-100">0 - 100</option>
                        <option value="101-500">101 - 500</option>
                        <option value="501-999">501 - 999</option>
                    </select>
                </td>
                <td valign="middle"><input style="width: 80px;" readonly="readonly"  type="text" id="6" class="products-search-input datepicker"></td>
                <td></td>
            </tr>
        </thead>
    </table>


    <div id="myInfoModal" class="modal fade">
        <div class="modal-dialog" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><b>Item Details</b></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="title">Tilte:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="title" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="part_number">Part Number:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="part_number" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="condition">Condition:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="condition" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="seller">Seller(Feedback/Solds):</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="seller" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="search_pattern">Search Pattern:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="search_pattern" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="search">Search Date:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="search" readonly>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="start_time">Start Time:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="start_time" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="end_time">End Time:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="end_time" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="quantiy">Quantiy(real/sold):</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="quantiy" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="price">Price:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="price" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="currency">Currency:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="currency" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="returns">Return Accepted:</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="returns" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


</div>



<script type="text/javascript" language="javascript" src="../js/app/basic.js"></script>