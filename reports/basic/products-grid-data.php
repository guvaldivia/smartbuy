<?php

require_once (dirname(dirname(dirname(__FILE__))).'/config.php');


/* Database connection start */
$servername = $GLOBALS['DB_HOST'];
$username = $_DB = $GLOBALS['DB_USER'];
$password = $GLOBALS['DB_PASSWD'];
$dbname = $GLOBALS['DB_NAME'];

$conn = mysqli_connect($servername, $username, $password, $dbname) or die("Connection failed: " . mysqli_connect_error());

/* Database connection end */


// storing  request (ie, get/post) global array to a variable
$requestData= $_REQUEST;


$columns = array(
// datatable column index  => database column name
    0 =>'title',
    1 => 'description',
    2 => 'start_time',
    3 => 'end_time',
    4 => 'quantity_sold',
    5 => 'price',
    6 => 'id',
    7 => 'part_number_id',
    8 => 'search.date',
    9 => 'currency'
);

// getting total number records without any search
$sql = "SELECT title, description, price, start_time, end_time, quantity_sold, item.id, part_number_id, search.date, currency";
$sql.=" FROM item, part_number, search WHERE item.part_number_id=part_number.id AND item.search_id=search.id";
$query=mysqli_query($conn, $sql) or die("products-grid-data.php: get product");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


$sql = "SELECT title, description, price, start_time, end_time, quantity_sold, item.id, part_number_id, search.date, currency";
$sql.=" FROM item, part_number, search WHERE item.part_number_id=part_number.id AND item.search_id=search.id";

// getting records as per search parameters
if( !empty($requestData['columns'][0]['search']['value']) ){   //name
    $sql.=" AND title LIKE '%".$requestData['columns'][0]['search']['value']."%' ";
}

if( !empty($requestData['columns'][1]['search']['value']) ){   //name
    $sql.=" AND description LIKE '%".$requestData['columns'][1]['search']['value']."%' ";
}

if( !empty($requestData['columns'][2]['search']['value']) ){   //name
    //$sql.=" AND start_time LIKE '%".$requestData['columns'][2]['search']['value']."%' ";
    $start_date =  date_create($requestData['columns'][2]['search']['value']);
    $start_date_formated = date_format($start_date, "Y-m-d") . " 00:00:00";

    $sql.=" AND start_time >= '".$start_date_formated."' ";
}

if( !empty($requestData['columns'][3]['search']['value']) ){   //name
    //$sql.=" AND end_time LIKE '%".$requestData['columns'][3]['search']['value']."%' ";
    $end_date =  date_create($requestData['columns'][3]['search']['value']);
    $end_date_formated = date_format($end_date, "Y-m-d") . " 23:59:59";

    $sql.=" AND end_time <= '".$end_date_formated."' ";
}

if( !empty($requestData['columns'][4]['search']['value']) ){   //name
    $sql.=" AND quantity_sold LIKE '%".$requestData['columns'][4]['search']['value']."%' ";
}

if( !empty($requestData['columns'][5]['search']['value']) ){   //name
    //$sql.=" AND price LIKE '%".$requestData['columns'][5]['search']['value']."%' ";
    $rangeArray = explode("-",$requestData['columns'][5]['search']['value']);
    $minRange = $rangeArray[0];
    $maxRange = $rangeArray[1];
    $sql.=" AND ( price >= '".$minRange."' AND  price <= '".$maxRange."' ) ";
}

if( !empty($requestData['columns'][6]['search']['value']) ){   //name

    $real_date = explode(" ", $requestData['columns'][6]['search']['value']);
    if ( count($real_date) == 2 ) {

        $start_date =  date_create($requestData['columns'][6]['search']['value']);
        $start_date_formated = date_format($start_date, "Y-m-d H:i:s");

        $sql.=" AND search.date = '".$start_date_formated."' ";

    } else {

        $rangeDate = explode("-", $requestData['columns'][6]['search']['value']);
        if ( count($rangeDate) == 2 ) {
            $start_date =  date_create($rangeDate[0]);
            $end_date =  date_create($rangeDate[1]);
            $start_date_formated = date_format($start_date, "Y-m-d") . " 00:00:00";
            $end_date_formated = date_format($end_date, "Y-m-d") . " 23:59:59";

            $sql.=" AND ( end_time >= '".$start_date_formated."' AND end_time <= '".$end_date_formated."' ) ";

        } else {
            $start_date =  date_create($requestData['columns'][6]['search']['value']);
            $start_date_formated = date_format($start_date, "Y-m-d") . " 00:00:00";
            $end_date_formated = date_format($start_date, "Y-m-d") . " 23:59:59";

            $sql.=" AND ( search.date >= '".$start_date_formated."' AND search.date <= '".$end_date_formated."' ) ";
        }
    }


}


$query=mysqli_query($conn, $sql) or die("products-grid-data.php: get products");
$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.
$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
/* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc  */
$query=mysqli_query($conn, $sql) or die("products-grid-data.php: get products");

//var_dump($sql);die;

$data = array();
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
    $nestedData=array();

    $start_date =  date_create($row["start_time"]);
    $end_date =  date_create($row["end_time"]);
    $date_appear =  date_create($row['date']);


    $nestedData[] = $row["title"];
    $nestedData[] = "<a href='#' onClick='editPartNumber(\"".$row["part_number_id"]."\", \"".$row["description"]."\")'>".$row["description"]."</a>";
    $nestedData[] = date_format($start_date, "m/d/Y H:i:s");
    $nestedData[] = date_format($end_date, "m/d/Y H:i:s");
    $nestedData[] = $row["quantity_sold"];
    $nestedData[] = $row["price"] . " " . $row["currency"];
    $nestedData[] = date_format($date_appear, "m/d/Y H:i:s");
    $nestedData[] = "<a href='#' class='info' data-value='".$row["id"]."' ><i class=\"fa fa-info\" aria-hidden=\"true\"></i></a> | <a href='#' onClick='alertDelete(\"".$row["id"]."\")'><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>";

    $data[] = $nestedData;
}



$json_data = array(
    "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
    "recordsTotal"    => intval( $totalData ),  // total number of records
    "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
    "data"            => $data   // total data array
);

echo json_encode($json_data);  // send data as json format

?>
