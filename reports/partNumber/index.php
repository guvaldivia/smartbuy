<div class="header"><h2>Basic Report on Part Numbers</h2></div>
<div class="container">

    <div class="panel panel-default">
        <div class="panel-heading">Global Filters</div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="filterFrom">From:</label>
                    <div class="col-sm-6">
                        <input readonly="readonly"  type="text" id="filterFrom" class="datepicker">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="filterTo">To:</label>
                    <div class="col-sm-6">
                        <input readonly="readonly"  type="text" id="filterTo" class="datepicker">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button class="btn btn-default" id="btnFilter">Filter</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <table id="partnumbers-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
        <thead>
            <tr>
                <th>Part Number</th>
                <th>Open</th>
                <th>Closed</th>
                <th>Total Items</th>
                <th>Total Solds</th>
                <th>Price(Min/Max/Mid)</th>
            </tr>
        </thead>
        <thead>
        <tr>
            <td></td>
            <td valign="middle"><input style="width: 80px;" readonly="readonly"  type="text" id="1" class="partnumber-search-input datepicker"></td>
            <td valign="middle"><input style="width: 80px;" readonly="readonly"  type="text" id="2" class="partnumber-search-input datepicker"></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </thead>
    </table>

</div>



<script type="text/javascript" language="javascript" src="../js/app/partNumber.js"></script>