<?php

require_once (dirname(dirname(dirname(__FILE__))).'/config.php');


/* Database connection start */
$servername = $GLOBALS['DB_HOST'];
$username = $_DB = $GLOBALS['DB_USER'];
$password = $GLOBALS['DB_PASSWD'];
$dbname = $GLOBALS['DB_NAME'];

$conn = mysqli_connect($servername, $username, $password, $dbname) or die("Connection failed: " . mysqli_connect_error());

/* Database connection end */


// storing  request (ie, get/post) global array to a variable
$requestData= $_REQUEST;


$columns = array(
// datatable column index  => database column name
    0 =>'description',
    1 => 'min_time',
    2 => 'max_time',
    3 => 'max_count',
    4 => 'total',
    5 => 'min_price',
    6 => 'max_price'
);

// getting total number records without any search
$sql = "SELECT q.* FROM ( SELECT part_number.description, MIN(item.start_time) AS min_time, MAX(item.end_time) AS max_time, COUNT(item.part_number_id) AS max_count, SUM(item.quantity_sold) AS total, MIN(item.price) AS min_price, MAX(item.price) AS max_price FROM part_number LEFT JOIN `item` ON item.part_number_id = part_number.id GROUP BY part_number.id ) q";
$query=mysqli_query($conn, $sql) or die("partnumbers-grid-data.php: get products");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


$sql = "SELECT q.* FROM ( SELECT part_number.description, MIN(item.start_time) AS min_time, MAX(item.end_time) AS max_time, COUNT(item.part_number_id) AS max_count, SUM(item.quantity_sold) AS total, MIN(item.price) AS min_price, MAX(item.price) AS max_price FROM part_number LEFT JOIN `item` ON item.part_number_id = part_number.id GROUP BY part_number.id ) q";
$sql .= " WHERE 1 = 1";

if( !empty($requestData['columns'][1]['search']['value']) ){
    $start_date =  date_create($requestData['columns'][1]['search']['value']);
    $start_date_formated = date_format($start_date, "Y-m-d") . " 00:00:00";

    $sql.=" AND q.min_time >= '".$start_date_formated."' ";
}

if( !empty($requestData['columns'][2]['search']['value']) ){

    $rangeDate = explode("-", $requestData['columns'][2]['search']['value']);
    if ( count($rangeDate) == 2 ) {
        $start_date =  date_create($rangeDate[0]);
        $end_date =  date_create($rangeDate[1]);
        $start_date_formated = date_format($start_date, "Y-m-d") . " 00:00:00";
        $end_date_formated = date_format($end_date, "Y-m-d") . " 23:59:59";

        $sql.=" AND ( q.max_time >= '".$start_date_formated."' AND q.max_time <= '".$end_date_formated."' ) ";

    } else {
        $end_date =  date_create($requestData['columns'][2]['search']['value']);
        $end_date_formated = date_format($end_date, "Y-m-d") . " 23:59:59";

        $sql.=" AND q.max_time <= '".$end_date_formated."' ";
    }

}


$query=mysqli_query($conn, $sql) or die("partnumber-grid-data.php: get products");
$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.
$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
//var_dump($sql);die;
/* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc  */
$query=mysqli_query($conn, $sql) or die("partnumber-grid-data.php: get partnumber");

$data = array();
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
    $nestedData=array();

    $start_date =  date_create($row['min_time']);
    $end_date =  date_create($row['max_time']);

    $mid = ($row['min_price'] + $row['max_price']) / 2;

    $nestedData[] = "<a href='?page=basic&part-number=".$row['description']."'>".$row['description']."</a>";
    $nestedData[] = date_format($start_date, "m/d/Y");
    $nestedData[] = date_format($end_date, "m/d/Y");
    $nestedData[] = intval($row['max_count']);
    $nestedData[] = intval($row['total']);
    $nestedData[] = $row['min_price'] ."/". $row['max_price'] . "/" . $mid;

    $data[] = $nestedData;
}



$json_data = array(
    "draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
    "recordsTotal"    => intval( $totalData ),  // total number of records
    "recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
    "data"            => $data   // total data array
);

echo json_encode($json_data);  // send data as json format

?>
