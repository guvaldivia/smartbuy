<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 1/4/17
 * Time: 12:14 AM
 */

require_once('lib/class.ebay.php');
require_once('lib/DB.php');
require_once('lib/utils.php');
require_once('config.php');

//Initiation Ebay conection
$ebay = new ebay();

//Initiation Utils tools
$utils = new Utils();

//Initiation of DB object
$db = new DB();


$timeZone = 'America/New_York';

foreach ($GLOBALS['FILTER_PATTERNS'] as $pattern) {

    $page = 1;

    $results = $ebay->findItemsCompleted($pattern, $page);

    //Search Pattern Setup
    $response = $db->simpleQuery(
        'search_pattern',               # Table
        array( '*' ),           		# Fields
        array(                  		# Filters
            'description' => $pattern
        )
    );

    if ( count($response) == 0 ) {

        $search_pattern_id = $utils->generateId();

        $response = $db->insert('search_pattern',       # Table
            array(                  					# Insert
                'id' => $search_pattern_id,
                'description' => $pattern
            )
        );
    } else {
        $search_pattern_id = $response[0]['id'];
    }


    //Insert the search defines into DB
    $search_id = $utils->generateId();
    $real_items = 0;

    $db->insert('search',					       		# Table
        array(                  						# Insert
            'id' => $search_id,
            'search_pattern_id' => $search_pattern_id,
            'count_items' => $real_items
        )
    );


    $total_page = $results['findCompletedItemsResponse'][0]['paginationOutput'][0]['totalPages'][0];

    echo "Total Pages: " . $total_page . "<br>";

    while ( $total_page >= $page ) {

        echo "Page: " . $page . "<br>";

        $results = $ebay->findItemsCompleted($pattern, $page);

        $items = $results['findCompletedItemsResponse'][0]['searchResult'][0]['item'];

        $count_item = 1;

        foreach($items as $i){

            $item_is_new = false;

            //General
            $title = $i['title'][0];
            $item_id = $i['itemId'][0];

            echo "Item: " . $count_item . ", Item ID: " . $item_id . "<br>";
            $count_item++;

            //Listing
            $startTime = $i['listingInfo'][0]['startTime'];
            $startTime = new DateTime($startTime[0], new DateTimeZone('GMT'));
            $startTime->setTimeZone(new DateTimeZone($timeZone));
            $startTime = date_format($startTime, "Y-m-d H:i:s");

            $endTime = $i['listingInfo'][0]['endTime'];
            $endTime = new DateTime($endTime[0], new DateTimeZone('GMT'));
            $endTime->setTimeZone(new DateTimeZone($timeZone));
            $endTime = date_format($endTime, "Y-m-d H:i:s");

            //var_dump($startTime);
            //var_dump($endTime);
            //die;

            //Search Pattern Setup
            $response = $db->simpleQuery(
                'item',               			# Table
                array( '*' ),           		# Fields
                array(                  		# Filters
                    'item_id' => $item_id
                )
            );

            if ( count($response) == 0 ) {
                $item_is_new = true;
            }

            //Category
            $primaryCategoryId = $i['primaryCategory'][0]['categoryId'];
            $primaryCategoryName = $i['primaryCategory'][0]['categoryName'];
            //$secondaryCategoryId = $i['secondaryCategory'][0]['categoryId'];
            //$secondaryCategoryName = $i['secondaryCategory'][0]['categoryName'];

            //Picture
            $galleryURL = $i['galleryURL'][0];

            //Payment Method
            $paymentMethod_array = $i['paymentMethod'];
            $paymentMethod = "";
            for ( $x=0; $x > count($paymentMethod_array); $x++ ) {
                $paymentMethod .= "," . $paymentMethod_array[$x];
            }

            //Location
            //$postalCode = $i['postalCode'][0];
            //$location = $i['location'][0];
            //$country = $i['country'][0];

            //shippingInfo
            if ( array_key_exists('shippingServiceCost', $i['shippingInfo'][0]) )
                $shippingServiceCost = $i['shippingInfo'][0]['shippingServiceCost'][0]['__value__'];
            else
                $shippingServiceCost = 0;
            $shippingType = $i['shippingInfo'][0]['shippingType'][0];
            $shipToLocations = $i['shippingInfo'][0]['shipToLocations'][0];


            //Pricing
            $currency =  $i['sellingStatus'][0]['currentPrice'][0]['@currencyId'];
            $currentPrice = $i['sellingStatus'][0]['currentPrice'][0]['__value__'];

            //returnsAccepted
            $returnsAccepted = $i['returnsAccepted'][0];

            //Condition
            $condition = $i['condition'][0]['conditionDisplayName'][0];


            $results = $ebay->findProductInfo($item_id);
            $product = $results['Item'];

            //Quantity
            $quantity = $product['Quantity'];
            $quantitySold = $product['QuantitySold'];

            //Seller Info
            $sellerUserId = $product['Seller']['UserID'];
            $sellerFeedbackPercent = $product['Seller']['PositiveFeedbackPercent'];

            //Hit Count
            if ( array_key_exists('HitCount', $product) )
                $HitCount = $product['HitCount'];
            else
                $HitCount = 0;

            //How many item are sold by this seller
            $sellerResults = $ebay->findItemsCompletedBySeller($sellerUserId);
            $itemsBySeller = $sellerResults['findCompletedItemsResponse'][0]['paginationOutput'][0]["totalEntries"][0];


            //Condition into DB
            if ( $condition == "" || $condition == null )
                $condition = "Undifined";

            $response = $db->simpleQuery(
                'condition_type',            	# Table
                array( '*' ),           		# Fields
                array(                  		# Filters
                    'description' => $condition
                )
            );

            if ( count($response) == 0 ) {

                $condition_id = $utils->generateId();

                $response = $db->insert('condition_type',       # Table
                    array(                  					# Insert
                        'id' => $condition_id,
                        'description' => $condition
                    )
                );
            } else {
                $condition_id = $response[0]['id'];
            }


            //Seller into DB
            $response = $db->simpleQuery(
                'seller', 	            		# Table
                array( '*' ),           		# Fields
                array(                  		# Filters
                    'user' => $sellerUserId
                )
            );

            if ( count($response) == 0 ) {

                $seller_id = $utils->generateId();

                $response = $db->insert('seller',       		# Table
                    array(                  					# Insert
                        'id' => $seller_id,
                        'user' => $sellerUserId,
                        'feedback' => $sellerFeedbackPercent,
                        'items_sold' => $itemsBySeller
                    )
                );
            } else {
                $seller_id = $response[0]['id'];

                $db->update('seller',							#Table
                    array(                  					# Update
                        'feedback' => $sellerFeedbackPercent,
                        'items_sold' => $itemsBySeller
                    ),
                    array(										#Filter
                        'id' => $seller_id
                    )
                );
            }


            //Gets Part Number
            $part_number = $utils->getPartNumber($title);


            //Part Number into DB
            $response = $db->simpleQuery(
                'part_number',            	# Table
                array( '*' ),           		# Fields
                array(                  		# Filters
                    'description' => $part_number
                )
            );

            if ( count($response) == 0 ) {

                $part_number_id = $utils->generateId();

                $response = $db->insert('part_number',       	# Table
                    array(                  					# Insert
                        'id' => $part_number_id,
                        'description' => $part_number
                    )
                );
            } else {
                $part_number_id = $response[0]['id'];
            }


            $returnsAcceptedConverted = ($returnsAccepted == 'true')?1:0;

            //Insert Product into Item Table
            $product_id = $utils->generateId();

            if ( $item_is_new ) {

                $response = $db->insert('item',       			# Table
                    array(                  					# Insert
                        'id' => $product_id,
                        'part_number_id' => $part_number_id,
                        'condition_type_id' => $condition_id,
                        'seller_id' => $seller_id,
                        'search_pattern_id' => $search_pattern_id,
                        'search_id' => $search_id,
                        'title' => $title,
                        'item_id' => $item_id,
                        'shipping' => $shippingServiceCost,
                        'start_time' => $startTime,
                        'end_time' => $endTime,
                        'quantity' => $quantity,
                        'quantity_sold' => $quantitySold,
                        'hit_count' => $HitCount,
                        'return_accepted' => $returnsAcceptedConverted,
                        'price' => $currentPrice,
                        'currency' => $currency
                    )
                );

            }
            else {

                //Updating real counter items
                $db->update('item',		# Table
                    array(
                        'part_number_id' => $part_number_id,
                        'condition_type_id' => $condition_id,
                        'seller_id' => $seller_id,
                        'search_pattern_id' => $search_pattern_id,
                        'search_id' => $search_id,
                        'end_time' => $endTime,
                        'quantity' => $quantity,
                        'quantity_sold' => $quantitySold,
                        'price' => $currentPrice,
                        'currency' => $currency
                    ),
                    array(                  # Filters
                        'item_id' => $item_id
                    )
                );

            }




            //Adding real items to the counter
            $real_items++;


            /*echo "------------------------------------INICIO-------------------------------------------------</br>";
            echo "Title: " . $title . "<br/>";
            echo "Item Id: " . $item_id . "<br/>";
            echo "Current Price: " . $currentPrice . " USD<br/>";
            echo "Shipping Type: " . $shippingType . "<br/>";
            echo "Shipping Cost: " . $shippingServiceCost . " USD<br/>";
            echo "Condition: " . $condition . "<br/>";
            echo "Start Time: " . $startTime . "<br/>";
            echo "End Time: " . $endTime . "<br/>";
            echo "Quantity: " . $quantity . "<br/>";
            echo "Quantity Sold: " . $quantitySold . "<br/>";
            echo "Hit Count: " . $HitCount . "<br/>";
            echo "Seller UserId: " . $sellerUserId . "<br/>";
            echo "Seller Feedback Percent: " . $sellerFeedbackPercent . "%<br/>";
            echo "Item sold by this Seller: " . $itemsBySeller . "<br/>";
            echo "Return Accepted: " . $returnsAccepted . "<br/>";
            echo 'Foto: <img src="'.$galleryURL.'"><br/>';
            echo "-------------------------------------FIN---------------------------------------------------</br>";*/

        }

        $page++;
    }

	//Updating real counter items
    $db->update('search',		# Table
        array(
            'count_items' => $real_items
        ),
        array(                  # Filters
            'id' => $search_id
        )
    );

}


